import update_conda
import shutil
import os.path as path
import os
import pytest

install_path = "test/conda"


def clear_conda_install():
    shutil.rmtree(install_path)


def check_installer_downloaded():
    files = os.listdir(path.join(install_path,"install"))
    for f in files:
        if f[:5] == "Mamba" and f[-3:] == ".sh":
            return
    assert False


def check_conda_installed():
    base = path.join(install_path, 'base')
    flag = path.join(base, 'installed.txt')

    assert path.exists(flag)
    assert path.exists(path.join(base,'bin/mamba'))


def test_update_conda():
    #clear_conda_install()
    update_conda.run("test/test_config.yaml", False)
    check_installer_downloaded()
    check_conda_installed()


def setup_unneeded_envs(deletemes):
    for deleteme in deletemes:
        print(deleteme)
        os.makedirs(deleteme, exist_ok=True)


def test_delete_conda():
    deleteme = path.join(install_path, "base", "envs", "deleteme")
    setup_unneeded_envs([deleteme])
    update_conda.run("test/test_config.yaml", False)
    assert not path.exists(deleteme)


def test_absent_link_double_booking():
    try:
        update_conda.run("test/test_config_absent_link.yaml", False)
        assert False
    except:
        assert True


#def test_bad_config():
 #   update_conda.run("test/bad.yaml", False)

if __name__ == "__main__":
    pytest.main()
